#!/bin/awk -f

BEGIN { RS="\n" ; FS = " " }

{
  if (!seen[$0]++ && length($0) > 1 && $7 > 170) {
    print $1, $2 ", wzrost: " $7;
  }
}

END {}
