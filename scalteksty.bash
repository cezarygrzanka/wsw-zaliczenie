#!/bin/bash

files=($@)
howMany=0

clear
echo "Input output filename: "
read outputFile

ifExists=`file $outputFile | awk '{ print $2 }'`

while [ "$ifExists" != "cannot" ]; do
  clear
  echo -e "File \x1b[33m$outputFile\x1b[0m already exists, choose another name: "
  read outputFile
  ifExists=`file $outputFile | awk '{ print $2 }'`
done


for file in ${files[*]}
do
  isFile=`file $file | awk '{ print $3 }'`

  if [ $isFile == "text" ]; then
    ((howMany=howMany+1))
    path="`pwd`/$file"

    prefix=$"\n##########################################\n\n$path\n\n##########################################\n"

    toSave="$prefix`cat $file`"
    echo -e "$toSave" >> $outputFile
  fi

done

if [ $howMany == 0 ]; then
  echo -e "\x1b[31mThere are now files to merge"
else
  echo -e "\x1b[32mI've merged $howMany file/'s"
fi
